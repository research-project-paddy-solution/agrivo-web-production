import axios, { AxiosError, AxiosResponse } from 'axios'
import { HttpMethods } from '../../types/enums/HttpMethods'
import ApiConstants from '../../constants/ApiConstants'

class FurrowIrrigationService {
    public async generateFurrowIrrigationPathsAsync(
        data: JSON
    ): Promise<AxiosResponse> {
        return new Promise<AxiosResponse>((resolve, reject) => {
            axios({
                url: `${ApiConstants.BASE_FURROW_IRRIGATION_CLOUD_URL}/v3/irrigations/generatePaths`,
                method: HttpMethods.Post,
                data,
            })
                .then((res: AxiosResponse) => {
                    resolve(res)
                })
                .catch((err: AxiosError) => {
                    reject(err)
                })
        })
    }
}

export default new FurrowIrrigationService()
