import * as React from 'react'
import { Formik, Form } from 'formik'
import Backdrop from '@mui/material/Backdrop'
import Box from '@mui/material/Box'
import Modal from '@mui/material/Modal'
import Fade from '@mui/material/Fade'
import theme from '../../../theme/hooks/CreateTheme'
import ParagraphBold from '../../atoms/typographies/ParagraphBold'
import Avatar from '@mui/material/Avatar'
import ContainedButton from '../../atoms/buttons/ContainedButton'
import UploadButton from '../../atoms/buttons/UploadButton'
import HeadLine4 from '../../atoms/typographies/HeadLine4'
import SelectField from '../../atoms/selectField/SelectFieldAtom'
import { AddResearchCategoryInitialValues } from './InitialValues'
import { AddResearchCategoryValidationSchema } from './ValidationSchema'

interface IProps {
    handleCancel(): void
    handleSave(): void
}

export default function OfficerAddNewResearchPaperModal({
    handleCancel,
    handleSave,
}: IProps) {
    const options = [
        { value: 1, label: 'Sample 1' },
        { value: 2, label: 'Sample 2' },
        { value: 3, label: 'Sample 3' },
        { value: 4, label: 'Sample 4' },
    ]

    return (
        <Modal
            aria-labelledby="transition-modal-title"
            aria-describedby="transition-modal-description"
            open={true}
            closeAfterTransition
            slots={{ backdrop: Backdrop }}
            slotProps={{
                backdrop: {
                    timeout: 500,
                },
            }}
        >
            <Fade in={true}>
                <Box sx={styles.box}>
                    <div style={{ textAlign: 'center', marginTop: -15 }}>
                        <HeadLine4
                            text={'officerAddNewResearchPaperModal.title'}
                            color={theme.palette.primary.main}
                        />
                    </div>
                    <div style={styles.images}>
                        <Avatar
                            alt="Research Image"
                            src="./images/analysis.png"
                            style={{
                                marginTop: 20,
                                width: 120,
                                height: 120,
                                objectFit: 'contain',
                            }}
                            variant="rounded"
                        />
                    </div>

                    <div style={{ textAlign: 'center' }}>
                        <ParagraphBold
                            text={'officerAddNewResearchPaperModal.subText'}
                            color={theme.palette.primary.main}
                        />
                        <div style={{ marginTop: 10 }}>
                            <UploadButton
                                title={
                                    'officerAddNewResearchPaperModal.uploadButtonText'
                                }
                                height={30}
                                color={theme.palette.white.main}
                                backgroundColor={theme.palette.primary.main}
                            />
                        </div>
                    </div>

                    <Formik
                        initialValues={AddResearchCategoryInitialValues}
                        validationSchema={AddResearchCategoryValidationSchema}
                        onSubmit={(values) => {
                            console.log(values)
                        }}
                    >
                        {({
                            errors,
                            touched,
                            handleChange,
                            handleBlur,
                            values,
                        }) => (
                            <Form>
                                <div style={styles.selectField1}>
                                    <SelectField
                                        label={'selectField.category'}
                                        placeholder={
                                            'selectField.selectCategory'
                                        }
                                        options={options}
                                        name="category"
                                        value={values.category}
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                        error={
                                            (errors.category &&
                                                touched.category) ||
                                            undefined
                                        }
                                        errorText={errors.category}
                                    />
                                </div>

                                {((errors.category && touched.category) ||
                                    undefined) && (
                                    <div style={{ marginTop: 60 }}></div>
                                )}

                                <div style={styles.selectField2}>
                                    <SelectField
                                        label={'selectField.subCategory'}
                                        placeholder={
                                            'selectField.selectSubCategory'
                                        }
                                        options={options}
                                        name="subCategory"
                                        value={values.subCategory}
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                        error={
                                            (errors.subCategory &&
                                                touched.subCategory) ||
                                            undefined
                                        }
                                        errorText={errors.subCategory}
                                    />
                                </div>

                                {((errors.subCategory && touched.subCategory) ||
                                    undefined) && (
                                    <div style={{ marginBottom: 50 }}></div>
                                )}

                                <div
                                    style={styles.button}
                                    className="btn-toolbar"
                                    role="toolbar"
                                    aria-label="Toolbar with button groups"
                                >
                                    <div
                                        className="btn-group"
                                        role="group"
                                        aria-label="First group"
                                        style={styles.buttonGroup}
                                    >
                                        <ContainedButton
                                            title={'containedButtonTitles.save'}
                                            color={theme.palette.white.main}
                                            backgroundColor={
                                                theme.palette.primary.main
                                            }
                                            width={100}
                                        />
                                    </div>
                                    <div
                                        className="btn-group"
                                        role="group"
                                        aria-label="Third group"
                                        style={styles.buttonGroup2}
                                    >
                                        <ContainedButton
                                            title={
                                                'containedButtonTitles.cancel'
                                            }
                                            color={theme.palette.white.main}
                                            backgroundColor={
                                                theme.palette.neutral.main
                                            }
                                            onClick={handleCancel}
                                            width={100}
                                        />
                                    </div>
                                </div>
                            </Form>
                        )}
                    </Formik>
                </Box>
            </Fade>
        </Modal>
    )
}

const styles = {
    box: {
        position: 'absolute',
        top: '50%',
        left: '50%',
        transform: 'translate(-50%, -50%)',
        bgcolor: theme.palette.white.main,
        boxShadow: 24,
        p: 4,
        borderRadius: 2,
        maxWidth: 400,
    },

    button: {
        marginTop: 40,
        justifyContent: 'center',
        alignSelf: 'center',
    },

    buttonGroup2: {
        marginRight: 10,
        marginLeft: 10,
        marginTop: 10,
    },

    buttonGroup: {
        marginRight: 10,
        marginLeft: 10,
        marginTop: 10,
    },

    images: {
        justifyContent: 'center',
        alignSelf: 'center',
        display: 'flex',
        marginBottom: 10,
    },

    selectField1: {
        marginTop: 40,
        display: 'flex',
        justifyContent: 'center',
    },

    selectField2: {
        marginTop: 40,
        display: 'flex',
        justifyContent: 'center',
    },
}
