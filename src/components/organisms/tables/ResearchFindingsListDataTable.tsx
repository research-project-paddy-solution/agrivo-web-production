import * as React from 'react'
import MUIDataTable from 'mui-datatables'
import { useTranslation } from 'react-i18next'
import { Box, IconButton } from '@mui/material'
import { getDataArrayByJson } from '../../../utils/datatable/TransformData'
import OfficerEditResearchCategoryModal from '../../modals/officer/OfficerEditResearchCategoryModal'

export default function ResearchFindingsListDataTable() {
    const [id, setId] = React.useState(null)

    const { t } = useTranslation()
    const options: any = {
        responsive: 'standard',
        rowsPerPageOptions: [5, 10, 15, 20],
        rowsPerPage: 5,

        onTableChange: (action: any, state: any) => {
            console.log(action)
            console.log(state)
        },
    }

    const [isOpen, setIsOpen] = React.useState(false)

    function handleClick(rId: any) {
        setIsOpen(!isOpen)
        setId(rId) // Set the selected item ID
    }

    const sampleData = [
        {
            id: 1,
            a1: 'Variability in maturity duration and yield of some popular rice varieties over climatic zone, season and location.',
            a2: 'Agronomy',
            a3: 'Agronomy-Sub',
            a4: '2022-05-10',
        },
        {
            id: 2,
            a1: 'Variability in maturity duration and yield of some popular rice varieties over climatic zone, season and location.',
            a2: 'Agronomy',
            a3: 'Agronomy-Sub',
            a4: '2022-05-10',
        },
    ]

    const data = getDataArrayByJson(sampleData)

    const columns = [
        t('DataTable.researchNameColumn'),
        t('DataTable.categoryColumn'),
        t('DataTable.subCategoryColumn'),
        t('DataTable.createdDateColumn'),
        {
            name: t('DataTable.actionColumn'),
            options: {
                customBodyRender: (
                    value: any,
                    tableMeta: any,
                    updateValue: any
                ) => {
                    const rId = data[tableMeta.rowIndex][0]
                    return (
                        <div>
                            <IconButton
                                onClick={() => {
                                    handleClick(rId)
                                }}
                            >
                                <img
                                    alt="Edit Icon"
                                    src="./images/editing.png"
                                    style={{
                                        width: 25,
                                        height: 25,
                                    }}
                                />
                            </IconButton>
                            {isOpen && id === rId && (
                                <OfficerEditResearchCategoryModal
                                    handleCancel={() => {
                                        handleClick(rId)
                                    }}
                                    handleSave={() => {
                                        handleClick(rId)
                                    }}
                                />
                            )}
                        </div>
                    )
                },
            },
        },
    ]

    return (
        <Box sx={styles.table}>
            <MUIDataTable
                title={t('DataTable.researchList')}
                data={data}
                columns={columns}
                options={options}
            />
        </Box>
    )
}

const styles = {
    table: {
        marginLeft: '20px',
        marginRight: '20px',
        marginTop: '20px',
        marginBottom: '10px',
    },
}
