import * as React from 'react'
import MUIDataTable from 'mui-datatables'
import { useTranslation } from 'react-i18next'
import { Box } from '@mui/material'
import { getDataArrayByJson } from '../../../utils/datatable/TransformData'

export default function FinalizedTimeScheduleDataTable() {
    const { t } = useTranslation()
    const options: any = {
        responsive: 'standard',
        rowsPerPageOptions: [5, 10, 15, 20],
        rowsPerPage: 5,

        onTableChange: (action: any, state: any) => {
            console.log(action)
            console.dir(state)
        },
    }

    const sampleData = [
        {
            a1: 'Arun',
            a2: '5',
            a3: '2',
            a4: '2022-05-10',
            a5: '1',
            a6: '2022-04-10',
            a7: '8.00 AM - 8.15 AM',
        },
        {
            a1: 'Arun',
            a2: '5',
            a3: '2',
            a4: '2022-05-10',
            a5: '1',
            a6: '2022-04-10',
            a7: '8.00 AM - 8.15 AM',
        },

        {
            a1: 'Arun',
            a2: '5',
            a3: '2',
            a4: '2022-05-10',
            a5: '1',
            a6: '2022-04-10',
            a7: '8.00 AM - 8.15 AM',
        },
    ]

    const data = getDataArrayByJson(sampleData)

    const columns = [
        t('DataTable.farmerNameColumn'),
        t('DataTable.areaInAcresColumn'),
        t('DataTable.riceVariantColumn'),
        t('DataTable.cultivationStartDateColumn'),
        t('DataTable.priorityColumn'),
        t('DataTable.allocatedDateColumn'),
        t('DataTable.allocatedTimeSlotColumn'),
    ]

    return (
        <Box sx={styles.table}>
            <MUIDataTable
                title={t('DataTable.finalizedTimeList')}
                data={data}
                columns={columns}
                options={options}
            />
        </Box>
    )
}

const styles = {
    table: {
        marginLeft: '20px',
        marginRight: '20px',
        marginTop: '20px',
        marginBottom: '10px',
    },
}
