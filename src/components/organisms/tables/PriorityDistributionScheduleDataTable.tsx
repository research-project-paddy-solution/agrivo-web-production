import * as React from 'react'
import MUIDataTable from 'mui-datatables'
import { useTranslation } from 'react-i18next'
import { Box } from '@mui/material'
import { getDataArrayByJson } from '../../../utils/datatable/TransformData'

export default function PriorityDistributionScheduleDataTable() {
    const { t } = useTranslation()
    const options: any = {
        responsive: 'standard',
        rowsPerPageOptions: [10, 15, 20],
        rowsPerPage: 10,

        onTableChange: (action: any, state: any) => {
            console.log(action)
            console.dir(state)
        },
    }

    const sampleData = [
        {
            a1: 'Arun',
            a2: '5',
            a3: '2',
            a4: '2022-05-10',
            a5: '1',
        },
        {
            a1: 'Arun',
            a2: '5',
            a3: '2',
            a4: '2022-05-10',
            a5: '1',
        },

        {
            a1: 'Arun',
            a2: '5',
            a3: '2',
            a4: '2022-05-10',
            a5: '1',
        },

        {
            a1: 'Arun',
            a2: '5',
            a3: '2',
            a4: '2022-05-10',
            a5: '1',
        },

        {
            a1: 'Arun',
            a2: '5',
            a3: '2',
            a4: '2022-05-10',
            a5: '1',
        },

        {
            a1: 'Arun',
            a2: '5',
            a3: '2',
            a4: '2022-05-10',
            a5: '1',
        },

        {
            a1: 'Arun',
            a2: '5',
            a3: '2',
            a4: '2022-05-10',
            a5: '1',
        },
        {
            a1: 'Arun',
            a2: '5',
            a3: '2',
            a4: '2022-05-10',
            a5: '1',
        },
        {
            a1: 'Arun',
            a2: '5',
            a3: '2',
            a4: '2022-05-10',
            a5: '1',
        },
        {
            a1: 'Arun',
            a2: '5',
            a3: '2',
            a4: '2022-05-10',
            a5: '1',
        },
    ]

    const data = getDataArrayByJson(sampleData)

    const columns = [
        t('DataTable.farmerNameColumn'),
        t('DataTable.areaInAcresColumn'),
        t('DataTable.riceVariantColumn'),
        t('DataTable.cultivationStartDateColumn'),
        t('DataTable.priorityColumn'),
    ]

    return (
        <Box sx={styles.table}>
            <MUIDataTable
                title={t('DataTable.priorityDistributionSchedule')}
                data={data}
                columns={columns}
                options={options}
            />
        </Box>
    )
}

const styles = {
    table: {
        marginLeft: '20px',
        marginRight: '20px',
        marginTop: '20px',
        marginBottom: '30px',
    },
}
