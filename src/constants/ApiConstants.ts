class ApiConstants {
    public BASE_CLOUD_API_URL =
        'https://evening-beyond-47163-55c2d7b8a484.herokuapp.com/api'
    public BASE_LOCAL_API_URL = 'http://localhost:8090/api'
    public PUBLIC_URL = 'public'
    public INDIVIDUAL_COMPONENT_LOCAL_URL = 'http://127.0.0.1:5000'
    public BASE_FURROW_IRRIGATION_CLOUD_URL =
        'https://peaceful-cliffs-84568-c505c6aba5e3.herokuapp.com'
}

export default new ApiConstants()
