import * as React from 'react'
import { useTranslation } from 'react-i18next'
import Box from '@mui/material/Box'
import Title from '../components/atoms/title/Title'
import ResearchFindingsListDataTable from '../components/organisms/tables/ResearchFindingsListDataTable'
import ContainedButton from '../components/atoms/buttons/ContainedButton'
import theme from '../theme/hooks/CreateTheme'
import OfficerAddNewResearchPaperModal from '../components/modals/officer/OfficerAddNewResearchPaperModal'
import { minHeight } from '@mui/system'

export default function ResearchFindingsPage() {
    const [isOpen, setIsOpen] = React.useState(false)
    function handleClick() {
        setIsOpen(!isOpen)
    }

    return (
        <Box sx={{ minHeight: 650 }}>
            <div>
                <Title backicon={false} titleName="titles.researchFindings" />
                <div style={styles.button}>
                    <ContainedButton
                        onClick={handleClick}
                        title={'containedButtonTitles.addNewPaper'}
                        backgroundColor={theme.palette.primary.main}
                        height={60}
                        width={200}
                    />
                </div>
            </div>

            <div>
                <ResearchFindingsListDataTable />
            </div>
            {isOpen && (
                <OfficerAddNewResearchPaperModal
                    handleCancel={handleClick}
                    handleSave={handleClick}
                />
            )}
        </Box>
    )
}

const styles = {
    button: {
        display: 'flex',
        justifyContent: 'end',
        marginRight: 20,
        marginTop: 20,
        marginBottom: 20,
    },
}
