import * as React from 'react'
import theme from '../theme/hooks/CreateTheme'
import Box from '@mui/material/Box'
import ContainedButton from '../components/atoms/buttons/ContainedButton'
import PriorityDistributionStepsCard from '../components/organisms/cards/PriorityDistibutionStepsCard'
import Title from '../components/atoms/title/Title'
import UploadButton from '../components/atoms/buttons/UploadButton'

export default function PriorityDistributionScheduleStepsPage() {
    function handleClick() {
        console.log('Button clicked!')
    }

    return (
        <Box sx={{ minHeight: 650 }}>
            <div>
                <Title
                    backicon={false}
                    titleName="titles.priorityDistributionSchedule"
                />
            </div>
            <div style={styles.card}>
                <div>
                    <PriorityDistributionStepsCard
                        steps={'STEP 01: '}
                        stepTitle={'Lorem ipsum dolor sit amet'}
                        description={
                            'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. '
                        }
                    />

                    <PriorityDistributionStepsCard
                        steps={'STEP 01: '}
                        stepTitle={'Lorem ipsum dolor sit amet'}
                        description={
                            'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. '
                        }
                    />

                    <PriorityDistributionStepsCard
                        steps={'STEP 01: '}
                        stepTitle={'Lorem ipsum dolor sit amet'}
                        description={
                            'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. '
                        }
                    />
                </div>
            </div>
            <div style={styles.button}>
                <UploadButton
                    title={'containedButtonTitles.upload'}
                    color={theme.palette.white.main}
                    backgroundColor={theme.palette.primary.main}
                />
            </div>
        </Box>
    )
}

const styles = {
    card: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: 10,
        marginRight: 10,
        marginTop: 20,
    },

    button: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 20,
        marginBottom: 20,
    },
}
