import * as React from 'react'
import Box from '@mui/material/Box'
import Title from '../components/atoms/title/Title'
import FinalizedTimeScheduleDataTable from '../components/organisms/tables/FinalizedTimeScheduleDataTable'
import theme from '../theme/hooks/CreateTheme'
import ContainedButton from '../components/atoms/buttons/ContainedButton'
import { useNavigate } from 'react-router-dom'

export default function FinalizedTimeSchedulePage() {
    const navigate = useNavigate()
    function handleClick() {
        console.log('Button clicked!')
    }
    return (
        <Box sx={{ minHeight: 650 }}>
            <div>
                <Title
                    backicon={true}
                    titleName="titles.finalzedTimeSchedule"
                    onClick={() => navigate('/priorityDistributionSchedule')}
                />
            </div>

            <div>
                <FinalizedTimeScheduleDataTable />
            </div>
            <div style={styles.button}>
                <ContainedButton
                    onClick={handleClick}
                    title={'containedButtonTitles.distributeQR'}
                    backgroundColor={theme.palette.primary.main}
                    width={200}
                />
            </div>
        </Box>
    )
}

const styles = {
    button: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 30,
        marginBottom: 20,
    },
}
