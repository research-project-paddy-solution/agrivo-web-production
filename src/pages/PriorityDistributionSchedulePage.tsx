import * as React from 'react'
import Box from '@mui/material/Box'
import Title from '../components/atoms/title/Title'
import theme from '../theme/hooks/CreateTheme'
import ContainedButton from '../components/atoms/buttons/ContainedButton'
import PriorityDistributionScheduleDataTable from '../components/organisms/tables/PriorityDistributionScheduleDataTable'
import { Grid } from '@mui/material'
import HeadLine4 from '../components/atoms/typographies/HeadLine4'
import dayjs, { Dayjs } from 'dayjs'
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs'
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider'
import { DateCalendar } from '@mui/x-date-pickers/DateCalendar'
import SelectField from '../components/atoms/selectField/SelectFieldAtom'
import { useNavigate } from 'react-router-dom'
import { Formik, Form } from 'formik'
import { PriorityDistributionScheduleInitialValues } from './InitialValues'
import { PriorityDistributionScheduleValidationSchema } from './ValidationSchema'

export default function PriorityDistributionSchedulePage() {
    const navigate = useNavigate()
    function handleClick() {
        console.log('Button clicked!')
    }
    const [value, setValue] = React.useState<Dayjs | null>(dayjs())

    const time = [
        { value: 1, label: '9.00 AM - 2.00 PM' },
        { value: 2, label: '10.00 AM - 1.30 PM' },
        { value: 3, label: '1.30 PM - 3.45 PM' },
    ]

    const location = [
        { value: 1, label: 'kalutara' },
        { value: 2, label: 'Panadura' },
        { value: 3, label: 'Horana' },
    ]

    return (
        <Box sx={{ minHeight: 650 }}>
            <div>
                <Title
                    backicon={true}
                    titleName="titles.priorityDistributionSchedule"
                    onClick={() => navigate('/priorityDistribution')}
                />
            </div>

            <Grid container>
                <Grid item xs={12} md={12} xl={9} lg={9}>
                    <div>
                        <PriorityDistributionScheduleDataTable />
                    </div>
                </Grid>
                <Grid item xs={12} md={12} xl={3} lg={3}>
                    <div
                        style={{
                            display: 'flex',
                            justifyContent: 'center',
                            marginBottom: 10,
                            textAlign: 'center',
                        }}
                    >
                        <HeadLine4
                            text={'priorityDistibutionSchedule.openingDates'}
                            color={theme.palette.primary.main}
                        />
                    </div>
                    <div
                        style={{
                            display: 'flex',
                            justifyContent: 'center',
                            marginBottom: 10,
                            textAlign: 'center',
                        }}
                    >
                        <LocalizationProvider dateAdapter={AdapterDayjs}>
                            <DateCalendar
                                defaultValue={dayjs()}
                                value={value}
                                onChange={(newValue) => setValue(newValue)}
                            />
                        </LocalizationProvider>
                    </div>

                    <Formik
                        initialValues={
                            PriorityDistributionScheduleInitialValues
                        }
                        validationSchema={
                            PriorityDistributionScheduleValidationSchema
                        }
                        onSubmit={(values) => {
                            console.log(values)
                        }}
                    >
                        {({
                            errors,
                            touched,
                            handleChange,
                            handleBlur,
                            values,
                        }) => (
                            <Form>
                                <div
                                    style={{
                                        display: 'flex',
                                        justifyContent: 'center',
                                        marginBottom: 10,
                                        textAlign: 'center',
                                    }}
                                >
                                    <HeadLine4
                                        text={
                                            'priorityDistibutionSchedule.openingHours'
                                        }
                                        color={theme.palette.primary.main}
                                    />
                                </div>

                                <div
                                    style={{
                                        display: 'flex',
                                        justifyContent: 'center',
                                        marginTop: 20,
                                        marginBottom: 30,
                                        marginLeft: 5,
                                        marginRight: 5,
                                    }}
                                >
                                    <SelectField
                                        label={'selectField.hours'}
                                        placeholder={'selectField.selectHours'}
                                        options={time}
                                        name="hours"
                                        value={values.hours}
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                        error={
                                            (errors.hours && touched.hours) ||
                                            undefined
                                        }
                                        errorText={errors.hours}
                                    />
                                </div>

                                {((errors.hours && touched.hours) ||
                                    undefined) && (
                                    <div style={{ marginTop: 50 }}></div>
                                )}

                                <div
                                    style={{
                                        display: 'flex',
                                        justifyContent: 'center',
                                        marginBottom: 10,
                                        textAlign: 'center',
                                    }}
                                >
                                    <HeadLine4
                                        text={
                                            'priorityDistibutionSchedule.individualTimeSlot'
                                        }
                                        color={theme.palette.primary.main}
                                    />
                                </div>

                                <div
                                    style={{
                                        display: 'flex',
                                        justifyContent: 'center',
                                        marginTop: 20,
                                        marginBottom: 30,
                                        marginLeft: 5,
                                        marginRight: 5,
                                    }}
                                >
                                    <SelectField
                                        label={'selectField.timeSlot'}
                                        placeholder={
                                            'selectField.selectTimeSlot'
                                        }
                                        options={time}
                                        name="timeSlot"
                                        value={values.timeSlot}
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                        error={
                                            (errors.timeSlot &&
                                                touched.timeSlot) ||
                                            undefined
                                        }
                                        errorText={errors.timeSlot}
                                    />
                                </div>

                                {((errors.timeSlot && touched.timeSlot) ||
                                    undefined) && (
                                    <div style={{ marginTop: 50 }}></div>
                                )}

                                <div
                                    style={{
                                        display: 'flex',
                                        justifyContent: 'center',
                                        marginBottom: 10,
                                        textAlign: 'center',
                                    }}
                                >
                                    <HeadLine4
                                        text={
                                            'priorityDistibutionSchedule.selectAgrarianDivision'
                                        }
                                        color={theme.palette.primary.main}
                                    />
                                </div>

                                <div
                                    style={{
                                        display: 'flex',
                                        justifyContent: 'center',
                                        marginTop: 20,
                                        marginLeft: 5,
                                        marginRight: 5,
                                    }}
                                >
                                    <SelectField
                                        label={'selectField.agraianDivision'}
                                        placeholder={
                                            'selectField.selectAgrarianDivision'
                                        }
                                        options={location}
                                        name="agrarianDivision"
                                        value={values.agrarianDivision}
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                        error={
                                            (errors.agrarianDivision &&
                                                touched.agrarianDivision) ||
                                            undefined
                                        }
                                        errorText={errors.agrarianDivision}
                                    />
                                </div>

                                {((errors.agrarianDivision &&
                                    touched.agrarianDivision) ||
                                    undefined) && (
                                    <div style={{ marginBottom: 40 }}></div>
                                )}
                                <div style={styles.button}>
                                    <ContainedButton
                                        onClick={() =>
                                            navigate('/finalizedTimeSchedule')
                                        }
                                        title={
                                            'containedButtonTitles.cteateSheet'
                                        }
                                        backgroundColor={
                                            theme.palette.primary.main
                                        }
                                        width={220}
                                        height={50}
                                    />
                                </div>
                            </Form>
                        )}
                    </Formik>
                </Grid>
            </Grid>
        </Box>
    )
}

const styles = {
    button: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 55,
        marginBottom: 40,
    },
}
